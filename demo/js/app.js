(function() {
    'use strict';
    
    angular.module('demoApp', ['ngObjectContext']);

    angular.module('demoApp').config(['objectContextProvider', function(objectContextProvider) {
        // Configure the object context provider here...
    }]);
})();
